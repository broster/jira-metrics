import re
import argparse
from calendar import day_abbr
from datetime import datetime, time, timedelta, timezone
from collections import defaultdict
from enum import Enum
from collections import namedtuple
from jira.client import JIRA


# TODO ignore weekends (evenings?) in date maths
# TODO pretty output
# TODO clever intervals? (i.e. not every 12 hours)
# TODO are issues that overlap sprints accounted for?
# TODO validate the maths
# TODO remove pickle code

class Status(Enum):
    TODO = 0
    BLOCKED = 1
    DEVELOPMENT = 2
    DONE = 3
    DROPPED = 4
    REVIEW = 5

    @staticmethod
    def from_str(status_string):
        mapper = {
            "To Do": Status.TODO,
            "TO DO": Status.TODO,
            "Blocked": Status.BLOCKED,
            "Development": Status.DEVELOPMENT,
            "Done": Status.DONE,
            "Dropped": Status.DROPPED,
            "Review": Status.REVIEW}
        return mapper[status_string]


class Label(Enum):
    BUG = "BUG"
    REVIEW = "REVIEW"
    MARKUP = "MARKUP"
    WIBNI = "WIBNI"


LIGHT = re.compile(".*\[?(?P<light>BLACK|RED|AMBER|GREEN)\]?(?P<summary>.*)")


class IssueType(Enum):
    STORY = 0
    BUG = 1
    EPIC = 2
    TASK = 3
    SUB_TASK = 4

    @staticmethod
    def from_issue_type(jira_issue_type):
        mapper = {
            "Story": IssueType.STORY,
            "Sub-task": IssueType.SUB_TASK,
            "Test Session (as sub-task)": IssueType.SUB_TASK,
            "Bug": IssueType.BUG,
            "Task": IssueType.TASK,
            "Epic": IssueType.EPIC}
        return mapper[jira_issue_type.name]


class Light(Enum):
    BLACK = 0
    RED = 1
    AMBER = 2
    GREEN = 3


StatusChange = namedtuple("StatusChange", "date old_status new_status")
LabelChange = namedtuple("LabelChange", "date old_labels new_labels")
TrafficLight = namedtuple("TrafficLight", "date light")

JiraDetails = namedtuple("jiraDetails", "server user password")

nested_dict = lambda: defaultdict(nested_dict)


def sub_task_method(func):
    def wrapper(self, *args, **kwargs):
        if self.jira_type != IssueType.SUB_TASK:
            raise RuntimeError("function not supported for {0}".format
                               (self.jira_type))
        return func(self, *args, **kwargs)
    return wrapper


def story_method(func):
    def wrapper(self, *args, **kwargs):
        if self.jira_type not in Sprint.STORIES:
            raise RuntimeError("function not supported for {0}".format
                               (self.jira_type))
        return func(self, *args, **kwargs)
    return wrapper


class Sprint:
    STORIES = [IssueType.STORY, IssueType.BUG]

    def __init__(self, sprint, issues):
        self._sprint = sprint
        self._issues = [Issue(i) for i in issues]

        self._report = nested_dict()
        self._start_date = None
        self._end_date = None

    def __getstate__(self):
        md = {}
        md["_issues"] = self._issues
        md["_start_date"] = self.start_date
        md["_end_date"] = self.end_date
        return md

    def __setstate__(self, state):
        self.__init__(None, [])
        for k, v in state.items():
            self.__dict__[k] = v

    @classmethod
    def from_jira(cls, jira, sprint_id):
        sprint = jira.sprint(sprint_id)
        issues = jira.search_issues("sprint = {0}".format(sprint.id),
                                    expand="changelog",
                                    maxResults=False)

        return cls(sprint, issues)

    @property
    def active(self):
        return self.end_date > datetime.now()

    @property
    def nominal_stories(self):
        return [i for i in self._issues if i.jira_type == IssueType.STORY]

    @property
    def bugs(self):
        return [i for i in self._issues if i.jira_type == IssueType.BUG]

    @property
    def stories(self):
        stories = [i for i in self._issues if i.jira_type in self.STORIES]
        for story in stories:
            story.update_sub_tasks(self._issues)
        return stories

    @property
    def avg_task_length(self):
        task_lengths = []
        for story in self.stories:
            task_lengths.extend(story.task_lengths())

        return sum(task_lengths, timedelta(0)) / len(task_lengths)

    @property
    def start_date(self):
        if not self._start_date:
            self._start_date = jiradate_to_datetime(self._sprint.startDate)
        return self._start_date

    @property
    def end_date(self):
        if not self._end_date:
            self._end_date = min([jiradate_to_datetime(self._sprint.endDate),
                                  datetime.now()]).replace(tzinfo=timezone.utc)
        return self._end_date

    def work_in_progress(self, date):
        return sum(story.work_in_progress(date) for story in self.stories)

    def dates(self, interval=12):
        """Return dates in scrum seperated by an interval"""
        next_datetime = datetime.combine(self.start_date.date(),
                                         time(0, tzinfo=timezone.utc))

        while next_datetime < self.end_date:
            yield next_datetime
            next_datetime += timedelta(hours=interval)

    def _add_per_date_results(self, date):
        results = self._report["stories"]
        for story in self.stories:
            results[story]["date"][date]["tl"] = story.traffic_light(date)
            results[story]["date"][date]["wip"] = story.work_in_progress(date)
        self._report["sprint"][date]["wip"] = self.work_in_progress(date)

    def _add_total_results(self):
        completed_stories = [s for s in self.stories if s.history.completed]
        results = self._report["stories"]
        for story in self.stories:
            results[story]["avg_task_len"] = story.average_task_length()
        for story in completed_stories:
            results[story]["duration"] = story.duration()

    def _generate_report(self):
        for date in self.dates():
            self._add_per_date_results(date)
        self._add_total_results()

    def report(self):
        self._generate_report()
        for story, data in self._report["stories"].items():
            print(story)
            print("- time to completion: {0}".format(
                data.get("duration", "ongoing")))
            print("- average task length: {0}".format(
                data.get("avg_task_len")))
            tl = ""
            wip = ""
            dow = ""
            for date in sorted(data["date"].keys()):
                dow += "\t{0}".format(day_abbr[date.weekday()])
                extra_data = data["date"][date]
                if extra_data["tl"]:
                    tl += "\t{0}".format(extra_data["tl"][0])
                else:
                    tl += "\t "
                wip += "\t{0}".format(extra_data["wip"])
            print(dow)
            print(tl)
            print(wip)

        wip = ""
        dow = ""
        for date in sorted(self._report["sprint"].keys()):
            dow += "\t{0}".format(day_abbr[date.weekday()])
            wip_info = self._report["sprint"][date]["wip"]
            wip += "\t{0}".format(wip_info)

        print("Sprint Data")
        print("- average task length: {0}".format(self.avg_task_length))
        print(dow)
        print(wip)

    def __str__(self):
        return "Sprint: {0}".format(self._sprint.name)


class Issue:
    def __init__(self, jira_issue):
        self._issue = jira_issue
        self._sub_tasks = None

        # Ensure the issue includes a changelog
        if not hasattr(self._issue, "expand") or "changelog" not in \
                self._issue.expand.split(","):
            self._issue.find(self._issue.key, params={"expand": "changelog"})

        # Cache values so the Issue can be pickled
        self._summary = None
        self._jira_type = None
        self._id = None
        self._history = None

    def __getstate__(self):
        md = {}
        md["_summary"] = self.summary
        md["_jira_type"] = self.jira_type
        md["_id"] = self.id
        md["_history"] = self.history
        return md

    @property
    def summary(self):
        if self._summary is not None:
            return self._summary

        try:
            grp = LIGHT.match(self._issue.fields.summary).groupdict()
            self._summary = grp["summary"].strip()
        except AttributeError:
            self._summary = self._issue.fields.summary

        return self._summary

    @classmethod
    def from_jira(cls, jira, name):
        return cls(jira.issue(name, expand="changelog"))

    @property
    def jira_type(self):
        if self._jira_type is None:
            self._jira_type = IssueType.from_issue_type(
                self._issue.fields.issuetype)
        return self._jira_type

    @property
    def sub_tasks(self):
        # Sub tasks haven't been added
        if self._sub_tasks is None:
            raise RuntimeError("Sub tasks not added")
        return self._sub_tasks

    @property
    def id(self):
        if self._id is None:
            self._id = self._issue.id
        return self._id

    @property
    def history(self):
        if self._history is None:
            self._history = History(self._issue.changelog.histories)
        return self._history

    @sub_task_method
    def status_change(self, date):
        """The last change in status before the specified date

        :param date: an offset-aware datetime.datetime object"""
        return last_object(self.history.statuses, date)

    @sub_task_method
    def status(self, date):
        """The status on a specific date

        :param date: an offset-aware datetime.datetime object"""
        status_change = self.status_change(date)
        if status_change is not None:
            return status_change.new_status
        return Status.TODO

    @story_method
    def traffic_light(self, date):
        """The traffic light on a specific date

        :param date: an offset-aware datetime.datetime object"""
        last_light = last_object(self.history.traffic_lights, date)
        if last_light:
            return last_light.light
        return None

    @story_method
    def duration(self):
        """The story duration

        :return: a datetime.timedelta
        """
        start_dates = []
        for task in self.sub_tasks:
            try:
                start_dates.append(task.history.start_date)
            except RuntimeError:
                pass

        if not start_dates:
            return timedelta(seconds=0)

        start = min(start_dates)
        finish = max(task.history.finish_date for task in self.sub_tasks)
        return finish - start

    @story_method
    def work_in_progress(self, date):
        """The number of subtasks that are in progress"""
        return sum(task.status(date) == Status.DEVELOPMENT for
                   task in self.sub_tasks)

    @story_method
    def update_sub_tasks(self, issues):
        # The sub_tasks stored as a field are not expanded. It's quicker to
        # replace them from a list of issues that have already been expanded
        self._sub_tasks = []
        for sub_task in self._issue.fields.subtasks:
            issue, = [i for i in issues if i.id == sub_task.id]
            self._sub_tasks.append(issue)

    @story_method
    def average_task_length(self):
        task_lengths = self.task_lengths()
        if task_lengths:
            return sum(task_lengths, timedelta(0)) / len(task_lengths)
        return None

    @story_method
    def task_lengths(self):
        task_lengths = []
        for task in self.sub_tasks:
            try:
                task_lengths.append(task.history.duration)
            except RuntimeError:
                # Ignore in progress tasks
                pass

        return task_lengths

    def __str__(self):
        return "Issue[{0}]: {1} {2}".format(str(
            self._issue), self._issue.fields.issuetype, self.summary)


class History:
    FINISHED = [Status.DONE, Status.DROPPED]

    def __init__(self, jira_histories):
        self._histories = jira_histories

        # Cache values so they can be pickled
        self._statuses = None
        self._traffic_lights = None
        self._labels = None

    def __getstate__(self):
        md = {}
        md["_statuses"] = self.statuses
        md["_traffic_lights"] = self.traffic_lights
        md["_labels"] = self.labels
        return md

    @staticmethod
    def history_date(history):
        # Convert date in format 2019-03-07T16:25:50.727+0000
        return jiradate_to_datetime(history.created)

    @property
    def statuses(self):
        if self._statuses is not None:
            return self._statuses

        statuses = []
        for history in self._histories:
            for item in history.items:
                if item.field == "status":
                    old_status = Status.from_str(item.fromString)
                    new_status = Status.from_str(item.toString)
                    date = self.history_date(history)
                    statuses.append(StatusChange(date, old_status, new_status))
        self._statuses = statuses
        return self._statuses

    @property
    def _last_status_change(self):
        statuses = self.statuses
        if not statuses:
            return None
        return max(self.statuses, key=lambda s: s.date)

    @property
    def start_date(self):
        """The first time a task enters development"""
        status_in_progress = [status for status in self.statuses if
                              status.new_status == Status.DEVELOPMENT]
        if len(status_in_progress) < 1:
            raise RuntimeError("Task never started")
        return min(status_in_progress, key=lambda s: s.date).date

    @property
    def finish_date(self):
        """The last time a task enters done or dropped"""
        if not self.completed:
            raise RuntimeError("Task not yet completed")
        status_finished = [status for status in self.statuses if
                           status.new_status in self.FINISHED]
        return max(status_finished, key=lambda s: s.date).date

    @property
    def duration(self):
        """raises a Runtime error if the story wasn't started or finished
        """
        return self.finish_date - self.start_date

    @property
    def completed(self):
        last_status_change = self._last_status_change
        if not last_status_change:
            return False
        return last_status_change.new_status in self.FINISHED

    @property
    def labels(self):
        if self._labels is not None:
            return self._labels

        labels = []
        for history in self._histories:
            for item in history.items:
                if item.field == "labels":
                    try:
                        old_labels = [
                            Label[l.upper()] for l in item.fromString.split()]
                        new_labels = [
                            Label[l.upper()] for l in item.toString.split()]
                    except KeyError:
                        # Unknown label - ignore
                        pass
                    else:
                        date = self.history_date(history)
                        labels.append(LabelChange(date, old_labels, new_labels))

        self._labels = labels
        return self._labels

    @property
    def traffic_lights(self):
        if self._traffic_lights is not None:
            return self._traffic_lights

        traffic_lights = []
        for history in self._histories:
            for item in history.items:
                if item.field == "summary":
                    match = LIGHT.match(item.toString)
                    if match:
                        light = match.groupdict()["light"]
                        date = self.history_date(history)
                        traffic_lights.append(TrafficLight(date, light))
        self._traffic_lights = traffic_lights
        return self._traffic_lights


def jiradate_to_datetime(jiradate):
    known_formats = ["%Y-%m-%dT%H:%M:%S.%f%z",
                     "%d/%b/%y %I:%M %p"]
    for frmt in known_formats:
        try:
            return datetime.strptime(jiradate, frmt)
        except ValueError as e:
            continue
    else:
        raise e


def last_object(objects, date):
    """The last object whose date is before the specified date

    :param objects: a list of any objects where `<object>.date` is an
        offset-aware datetime.datetime object
    :param date: an offset-aware datetime.datetime object"""
    last_obj = None
    last_obj_delta = None
    for obj in objects:
        time_delta = (obj.date - date).total_seconds()
        if time_delta < 0:
            if last_obj is None or time_delta > last_obj_delta:
                last_obj = obj
                last_obj_delta = time_delta
    return last_obj


def _get_credentials():
    parser = argparse.ArgumentParser()
    parser.add_argument("url", help="The URL of your Jira server ")
    parser.add_argument("user", help="Your Jira username")
    parser.add_argument("password", help="Your Jira password")
    parser.add_argument("sprint_id", help="The ID of the sprint")
    return parser.parse_args()


if __name__ == "__main__":
    args = _get_credentials()
    jira_options = {'server': args.url}
    jira = JIRA(options=jira_options, basic_auth=(args.user, args.password))
    sprint = Sprint.from_jira(jira, args.sprint_id)
    sprint.report()
