import threading
from flask import Flask, render_template, redirect
from datetime import datetime, timedelta

import instance.config as icfg
from models import refresh_data
from metrics import JiraDetails

app = Flask(__name__)

REFRESH_DATE = None
REFRESH_IN_PROGRESS = False


@app.route('/')
def home():
    columns = 5
    if len(icfg.SPRINT_INFO) != 0:
        for ii in range(columns - (len(icfg.SPRINT_INFO) % columns)):
            name = " " * ii
            icfg.SPRINT_INFO[name] = {
                icfg.JIRA_ID: None,
                icfg.IMG_URL: icfg.QUESTIONMARK_URL
            }
    return render_template("index.tmpl", sprints=icfg.SPRINT_INFO,
                           columns=columns)


@app.route('/statistics/sprint/<name>')
def sprint_stats(name):
    name = name.replace("%20", " ")
    jira_id = icfg.SPRINT_INFO[name][icfg.JIRA_ID]
    stats_to_include = [
        "/static/datetime-heatmap-{0}.html".format(jira_id)
    ]
    return render_template("sprint_stats.tmpl", sprint_name=name,
                           stats_urls=stats_to_include)


@app.route("/refresh")
def refresh():
    if REFRESH_IN_PROGRESS:
        return render_template("index_redirect.tmpl",
                               title="Refresh",
                               heading="Already refreshing!",
                               message="The refresh is currently in progress",
                               error=True)

    thread = _maybe_refresh(force=True)
    try:
        thread.join()
    except Exception as e:
        return render_template("index_redirect.tmpl",
                               title="Refresh",
                               heading="Refresh Failed!",
                               message=str(e),
                               error=True)

    return render_template("index_redirect.tmpl",
                               title="Refresh",
                               heading="Refresh Completed!",
                               message='All statistics now refreshed',
                               error=False)


def _maybe_refresh(force=False):
    REFRESH_IN_PROGRESS = True
    thread = None
    if force or datetime.now() - REFRESH_DATE < timedelta(hours=2):
        jira_details = JiraDetails(
            icfg.JIRA_SERVER, icfg.JIRA_USER, icfg.JIRA_PASSWORD)
        sprint_ids = [info[icfg.JIRA_ID] for info in icfg.SPRINT_INFO.values()]
        sprint_ids = [id for id in sprint_ids if id is not None]
        thread = threading.Thread(
            target=refresh_data,
            args=(sprint_ids, jira_details,
                  "webapp/static/datetime-heatmap-{0}.html", force))
        thread.start()
        REFRESH_DATE = datetime.now()

    REFRESH_IN_PROGRESS = False
    return thread


_maybe_refresh(force=True)
