import textwrap
import plotly.graph_objs as go
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
init_notebook_mode(connected=True)
# TODO notebook mode?

from metrics import JIRA, Sprint


# Convert traffic light tags to integers for the heatmap
TRAFFIC_LIGHT_MAP = {
    "BLACK": 4,
    "RED": 3,
    "AMBER": 2,
    "GREEN": 1,
    None: 0}

# White, Green, Amber, Red, Black,
TRAFFIC_LIGHT_RGB = {
    0: 'rgb(255, 255, 255)',
    1: 'rgb(0, 255, 0)',
    2: 'rgb(255, 255, 0)',
    3: 'rgb(255, 0, 0)',
    4: 'rgb(0, 0, 0)'
}

# THe interval to check traffic light status in hours
TRAFFIC_LIGHT_INTERVAL = 1


def _traffic_light_data(sprint):
    stories = list(sprint.stories)

    z = []
    for story in stories:
        z.append([TRAFFIC_LIGHT_MAP[story.traffic_light(date)] for date in
                  sprint.dates(TRAFFIC_LIGHT_INTERVAL)])

    data = [
        go.Heatmap(
            z=z,
            x=list(sprint.dates(TRAFFIC_LIGHT_INTERVAL)),
            y=["<br>".join(textwrap.wrap(s.summary, 40)) for s in stories],
            colorscale=_traffic_light_colours(z))
        ]
    return data


def _traffic_light_colours(data):
    all_lights = [tl for story in data for tl in story]
    colours = []
    entries = max(all_lights) - min(all_lights)
    for tl in range(min(all_lights), max(all_lights) + 1):
        colours.append([tl / entries, TRAFFIC_LIGHT_RGB[tl]])
    print(colours)
    print(set(all_lights))
    return colours


def _traffic_light_heatmap(data, file_name):
    """Create a HTML file with the the heatmap"""
    layout = go.Layout(
        title='Traffic Light Status',
        xaxis=dict(ticks='', nticks=14),
        yaxis=dict(ticks=''),
        margin=go.layout.Margin(l=250))

    fig = go.Figure(data=data, layout=layout)
    plot(fig, filename=file_name, auto_open=False)


def create_heatmap(sprint, file_name):
    data = _traffic_light_data(sprint)
    _traffic_light_heatmap(data, file_name)


def refresh_data(sprint_ids, jira_details, partial_path, force=False):
    """Create a HTML heatmap for each sprint

    :param sprint_ids: The list of Sprints
    :param jira_details: JiraDetails for the server
    :param partial_path: Location for HTML heatmap, will have sprint ID added,
                e.g. "dir/file-{0}"
    """
    jira = JIRA(options={'server': jira_details.server},
                basic_auth=(jira_details.user, jira_details.password))

    for sprint_id in sprint_ids:
        print(sprint_id)
        sprint = Sprint.from_jira(jira, sprint_id)
        if force or sprint.active:
            sprint.report()
            print("reported")
            create_heatmap(sprint, partial_path.format(sprint_id))
            print("created")